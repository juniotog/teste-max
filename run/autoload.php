<?php
function autoload($class_name) {
    $directories = array(
        APP_PATH,
        RUN_PATH,
        PUBLIC_PATH,
        CONTROLLER_PATH,
        MODEL_PATH
    );
    foreach ($directories as $directory) {
        if (file_exists($directory . $class_name . '.php')) {
            require_once($directory . $class_name . '.php');
            return;
        }       
    }
}
spl_autoload_register('autoload');