<?php
class Framework
{
    static $database = null;

    public static function run() {
        self::init();
        self::autoload();
        self::includeConfigs();
        self::dispatch();
    }
 
    private static function init() {
        define("DS", DIRECTORY_SEPARATOR);
        define("ROOT", getcwd() . DS);
        define("APP_PATH", ROOT . 'app' . DS);
        define("RUN_PATH", ROOT . "run" . DS);
        define("PUBLIC_PATH", ROOT . "public" . DS);
        define("CONFIG_PATH", APP_PATH . "config" . DS);
        define("CONTROLLER_PATH", APP_PATH . "controllers" . DS);
        define("MODEL_PATH", APP_PATH . "models" . DS);
        define("VIEW_PATH", APP_PATH . "views" . DS);
        define('BASE_URL', $_SERVER['SERVER_NAME'].str_replace('index.php', null, $_SERVER['PHP_SELF']));
        define('ASSETS', str_replace('index.php', null, $_SERVER['PHP_SELF']));
        session_start();
    }
   
    private static function autoload() {
        spl_autoload_register(array(__CLASS__,'load'));
    }
    
    private static function includeConfigs() {
        include CONFIG_PATH . "routers.php";
    }

    private static function load($class) {
        $directories = array(
            APP_PATH,
            RUN_PATH,
            PUBLIC_PATH,
            CONTROLLER_PATH,
            MODEL_PATH,
            RUN_PATH.'core'.DS,
            RUN_PATH.'database'.DS,
            RUN_PATH.'helpers'.DS,
            RUN_PATH.'libraries'.DS
        );
        foreach ($directories as $directory) {
            if (file_exists($directory . $class . '.php')) {
                require_once($directory . $class . '.php');
                return;
            }
            else if (file_exists($directory . $class . '.class.php')) {
                require_once($directory . $class . '.class.php');
                return;
            }       
        }
    }
 
    private static function dispatch() {
 
    }
}