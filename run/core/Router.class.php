<?php
class Router
{
    private static $routers = [];
    static $currentUrl;
    static function add($method, $url, $controller) {
        self::$routers[] = [
            'method' => $method,
            'url' => $url,
            'controller' => $controller
        ];
    }

    static function init() {
        if (isset($_GET['url'])) {
            self::$currentUrl = $_GET['url'];
            $prefix = explode('/', self::$currentUrl);
            $router = [];

            if (isset($prefix[0])) {
                foreach (self::$routers as $route) {
                    if (explode("/", $route['url'])[0] == $prefix[0]) {
                        if (strpos($route['url'], $prefix[count($prefix)-1]) !== false && strtolower($_SERVER['REQUEST_METHOD']) == strtolower($route['method'])) {
                            $router = $route;
                            break;
                        } else if($_SERVER['REQUEST_METHOD'] == "GET" && explode("@", $route['controller'])[1] == 'index') {
                            $router = $route;
                        } else if($_SERVER['REQUEST_METHOD'] == "POST" && explode("@", $route['controller'])[1] == 'store') {
                            $router = $route;
                        }
                    }
                }

                if($router) {

                
                    $params = preg_match_all('/{(.*?)}/', $router['url'], $match);
                    $positions = [];
                    if(count($match[1]) > 0) {
                    
                        foreach($match[1] as $params) {
                            $partials = explode("/", $router['url']);

                            foreach($partials as $key => $partial) {
                                if(strpos($partial, $params) !== false) {
                                    $positions[] = $prefix[$key];
                                }
                            }
                        }
                    }

                    list($controller, $action) = explode("@", $route['controller']);
                    $controller = new $controller;
                    call_user_func_array(array($controller, $action), $positions);
                }

            }
        }
    }
}