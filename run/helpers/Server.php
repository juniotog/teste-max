<?php
class Server {
    static function Uptime() {

        $uptime = @file_get_contents( "/proc/uptime");

        $uptime = explode(" ",$uptime);
        $uptime = $uptime[0];
        $days = explode(".",(($uptime % 31556926) / 86400));
        $hours = explode(".",((($uptime % 31556926) % 86400) / 3600));
        $minutes = explode(".",(((($uptime % 31556926) % 86400) % 3600) / 60));
        $seconds = explode(".",((((($uptime % 31556926) % 86400) % 3600) / 60) / 60));

        $time = $days[0].":".$hours[0].":".$minutes[0].":".$seconds[0];

        return $time;

    }
}