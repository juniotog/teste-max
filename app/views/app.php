<!DOCTYPE html>
<html lang="en" ng-app="MaxMilhas">
  <head>
    <base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Teste - MaxMilhas</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container" ng-controller="BlackListController">
        <div class="page-header">
            <h1>Black List</h1>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Consultar CPF</h3>
                    </div>
                    <div class="panel-body">
                        <form ng-submit="Search()">
                            <div class="form-group">
                              <label for="inputCpf">CPF</label>
                              <input type="text" class="form-control" required id="inputCpf" mask='999.999.999-99' ng-model="model.cpf"  placeholder="Digite o cpf">
                            </div>
                            <button type="submit" class="btn btn-default">Buscar</button>
                          </form>
                    </div>
                </div>
            
            </div>
        </div>

        <div class="row" ng-if="status.cpfNaoConsta">
            <div class="col-sm-12">
                <div class="well">
                    <h2>{{status.mensagem}}</h2>
                    <p>O CPF <strong>{{ model.cpf }}</strong> <font color=red>não consta</font> na blacklist, para adiciona-lo clique no botão abaixo.</p>
                    <button class="btn btn-default" type="submit" ng-click="Adicionar()">Adicionar CPF</button>
                </div>
            </div>
        </div>

        <div class="row" ng-if="status.cpfConsta">
            <div class="col-sm-12">
                <div class="well">
                    <h2>{{status.mensagem}}</h2>
                    <p>O CPF <strong>{{ model.cpf }}</strong> <font color=blue>consta</font> na blacklist, para remove-lo clique no botão abaixo.</p>
                    <button class="btn btn-primary" type="submit" ng-click="Remover()">Remover CPF</button>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="<?php echo ASSETS; ?>bower_components/ngMask/dist/ngMask.min.js"></script>
    <script src="<?php echo ASSETS; ?>bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
    <script>
        window.baseUri = 'http://<?php echo BASE_URL; ?>';

        
    </script>
    <script src="<?php echo ASSETS; ?>public/js/app.js"></script>
  </body>
</html>