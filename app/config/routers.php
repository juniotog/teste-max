<?php
/*
 * Router::add
 * 
 * first argument: method request
 * second argument: url
 * tree argument: controler@action
 */
Router::add('get', 'status', 'StatusController@index'); // Página principal
Router::add('get', 'cep', 'CpfController@index'); // Página principal
Router::add('post', 'cep', 'CpfController@store');  // Criar
Router::add('get', 'cep/{id}/{nome}/search', 'CpfController@search'); //Pesquisar
Router::add('get', 'cep/{id}/delete', 'CpfController@delete'); // Remove
Router::init();