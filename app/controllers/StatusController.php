<?php
/**
 * CpfController
 */
class StatusController {

    public $model = null;

    public function __construct() {
        $this->model = new BlackList();
    }


    public function index() {
        echo json_encode([
            'Uptime' => Server::Uptime(),
            'CPFs' => count($this->model->all()),
        ]);
    }
}