<?php
/**
 * CpfController
 */
class CpfController {

    public $model = null;

    public function __construct() {
        $this->model = new BlackList();
    }

    public function delete($cpf) {
        $cpf = OnlyNumber::only($cpf);
        $deleted = $this->model->delete($cpf);

        if($deleted == false) {
            Http::statusCode(403);
            echo json_encode([
                'mensagem' => 'Não foi possivel deletar o CPF'
            ]);
        } else {
            Http::statusCode(200);
            echo json_encode([
                'mensagem' => 'CPF deletado com sucesso.'
            ]);
        }
    }

    public function search($cpf) {
        $cpf = OnlyNumber::only($cpf);
        $exists = $this->model->getById($cpf);

        if (!empty($exists)) {
            Http::statusCode(200);
            echo json_encode([
                'mensagem' => 'CPF consta na black list'
            ]);
        } else {
            Http::statusCode(404);
            echo json_encode([
                'mensagem' => 'CPF não consta na black list'
            ]);
        }

    }

    public function store() {
        $data = json_decode(file_get_contents('php://input'), true);
        $cpf = OnlyNumber::only($data['cpf']);

        $exists = $this->model->getById($cpf);

        if (empty($exists)) {
            $this->model->insert([
                'cpf' => $cpf
            ]);
            Http::statusCode(201);
            echo json_encode([
                'mensagem' => 'CPF adicionado com sucesso.'
            ]);
        } else {
            Http::statusCode(302);
            echo json_encode([
                'mensagem' => 'CPF já consta na blacklist'
            ]);
        }
    }


    public function index() {
        include(VIEW_PATH . "app.php");
    }
}