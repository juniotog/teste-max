<?php 
class BlackList extends Model {
    
    public $fields = ['cpf'];
    public $primary = 'cpf';

    public function __construct() {
        parent::__construct();
        $this->model = 'blacklist';
    }
}