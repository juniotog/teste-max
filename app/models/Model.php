<?php 
class Model {
    public $database = null;
    public $model = null;
    public $fields = [];
    public $primary = 'id';

    public function __construct() {
        try {
            $this->database = new SQLite();
            Database::createTables($this->database);
        } catch(Exception $ex) {
            echo $ex->getMessage();
            die;
        }
    }

    public function delete($id) {
        $sql = "DELETE from `{$this->model}` where  `$this->primary` = $id";
        $ret = $this->database->exec($sql);
        if ($ret){
            return $this->database->changes();
        }
        return false;
    }

    public function getById($id) {

        $ret = $this->database->query("select * from `{$this->model}` where `$this->primary`=$id");
        $row = $ret->fetchArray(SQLITE3_ASSOC);
        return $row;
    }

    public function insert($list){
        $field_list = ''; 
        $value_list = '';  

        foreach ($list as $k => $v) {
            if (in_array($k, $this->fields)) {
                $field_list .= "`".$k."`" . ',';
                $value_list .= "'".$v."'" . ',';
            }
        }
        $field_list = rtrim($field_list,',');
        $value_list = rtrim($value_list,',');

        $sql = "INSERT INTO `{$this->model}` ({$field_list}) VALUES ($value_list)";

        if ($this->database->exec($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function all() {

        $ret = $this->database->query("SELECT * FROM ".$this->model);
        $return = [];
        while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {
            $return[] = $row;
        }
        return $return;
    }
}