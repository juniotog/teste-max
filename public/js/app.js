var app = angular.module("MaxMilhas", ['ngMask'])
.service('BlackListService', ['$http', function ($http) {


    this.store = function (data) {
        return $http.post(baseUri+"cep", data);
    };

    this.delete = function (cpf) {
        return $http.get(baseUri+"cep/"+cpf+"/delete");
    };
    
    this.getByCPF = function (cpf) {
        return $http.get(baseUri+"cep/"+cpf+"/search");
    };


}])
.controller("BlackListController", function($scope, BlackListService) {

    $scope.model = {
        cpf: null,
    };
    $scope.status = {
        cpfNaoConsta: false,
        cpfConsta: false,
    };

    function resetStatus() {
        $scope.status.cpfNaoConsta = false; 
        $scope.status.cpfConsta = false; 
    }

    $scope.Adicionar = function () {
        BlackListService.store($scope.model).then(function (response) {
            $.notify({
                title: '<strong>Adicionado!</strong>',
                message: response.data.mensagem
            },{
                type: 'success'
            });
            
        }, function (response) {
            $.notify({
                title: '<strong>Falha!</strong>',
                message: response.data.mensagem
            },{
                type: 'danger'
            });
        });
    };

    $scope.Remover = function () {

        BlackListService.delete($scope.model.cpf).then(function (response) {
            $.notify({
                title: '<strong>Removido!</strong>',
                message: response.data.mensagem
            },{
                type: 'success'
            });
            
        }, function (response) {
            $.notify({
                title: '<strong>Falha!</strong>',
                message: response.data.mensagem
            },{
                type: 'danger'
            });
        });
    };

    $scope.Search = function () {
        resetStatus();
        BlackListService.getByCPF($scope.model.cpf).then(function (response) {
            $scope.status.cpfConsta = true;
            $scope.status.mensagem = response.data.mensagem;
        }, function (response) {
            $scope.status.cpfNaoConsta = true;
            $scope.status.mensagem = response.data.mensagem;
        });
    }


});
